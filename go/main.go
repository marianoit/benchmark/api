package main

import (
    "fmt"
    "log"
    "net/http"
)

func helloWorld(w http.ResponseWriter, r *http.Request){
    fmt.Fprintf(w, "Hello World!")
}

func handleRequests() {
    http.HandleFunc("/", helloWorld)
    log.Fatal(http.ListenAndServe(":5503", nil))
}

func main() {
    handleRequests()
}