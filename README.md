Para iniciar el docker se ejecuta el run.sh de cada plataforma o se construyen todos los contenedores con docker-compose

> sudo docker-compose build 
> sudo docker-compose up -d 

## Flask

ab -n 15000 -c 100 http://localhost:5500/

## dotnet 6 core con API minima

ab -n 15000 -c 100 http://localhost:5501/

## Express

ab -n 15000 -c 100 http://localhost:5502/

## GO

ab -n 15000 -c 100 http://localhost:5503/
